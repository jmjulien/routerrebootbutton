-- Define our led pins and set them as output
BUTTON_LED_PIN = 1
RED_LED_PIN = 3
gpio.mode(BUTTON_LED_PIN, gpio.OUTPUT)
gpio.mode(RED_LED_PIN, gpio.OUTPUT)

-- Turn them on by default so we know that we are in
-- the starting sequence
gpio.write(BUTTON_LED_PIN, gpio.HIGH)
gpio.write(RED_LED_PIN, gpio.LOW)

-- wait 4 seconds before starting so we have time to stop the this
-- timer or delete this file (init.lua) if something goes wrong
-- this is to prevent the need to reflash the whole firmware if there is a lock-up
tmr.alarm(0, 4000, 1, function ()
    dofile("main.lua")
end)
