
-- Various modes of the system (our micro state machine)
-- MODE = 0  -- batt low
MODE = 1  -- not conected
-- MODE = 2  -- connected
-- MODE = 3  -- Done

-- the function to check the battery voltage
function checkbattery ()
    -- 500 is around 2.8V with our voltage divider circuit
    if adc.read(0) < 500 then
        MODE = 0
    end
end

--Load networking stuff
dofile("network.lua")

--Led state 
value = true
-- 0.25 sec period count rollover every 2 seconds (0-7)
period = 0

-- This is our main loop
-- interrupt every 0.25 sec. 
tmr.alarm(0, 250, 1, function ()
    
    -- check statuses every 2 sec
    if period == 0 then
        networkstatus()
        checkbattery()  --Comment this line to disable the low voltage detection
        checkreboot()
    end
    
    -- Set the leds to value 
    gpio.write(BUTTON_LED_PIN, value and gpio.HIGH or gpio.LOW)
    gpio.write(RED_LED_PIN, value and gpio.LOW or gpio.HIGH)

    -- Start the led blink pattern based on MODE
    -- adjust value based on period and mode
    if MODE == 1 then
        -- not connected we blink every second
        if period == 0 or period == 4 then
            value = not value
        end
    elseif MODE == 0 then
        -- if battery too low we blink fast every .25 sec
        value = not value
    elseif MODE == 2 then
        -- connected leds stays on 
        value = true
    elseif MODE == 3 then
        -- once finished, leds stays off
        value = false
    end
    
    --increment the period 0.25 sec
    period = period + 1
    -- rollback period counter every 2 sec
    if period == 8 then
        period = 0
    end
    
end)


