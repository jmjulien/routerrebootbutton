-- Credentials

-- wifi
SSID = "MIDDLEEARTH"
PASSWORD = "frodobaggins"

-- telnet
USER = "admin"
PWD = "password"
ROUTERIP = "192.168.0.1"

-- telnet interaction console (case sensitive)
-- these are words searched in router telnet responses to figure out when and what to answer.
SEARCH_USER = "login"
SEARCH_PWD = "Password"
SEARCH_PROMPT = "#"

--Reboot command for your router
REBOOT = "reboot"