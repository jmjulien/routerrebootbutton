-- Load credentials from the filesystem
dofile("credentials.lua")

-- How many times do we want to reboot
reboot_count = 5

-- period of wait between reboots
reboot_period = 60

-- Set ourselves as a wifi client
wifi.setmode(wifi.STATION)

-- Set credentials and auto connect
wifi.sta.config(SSID, PASSWORD)

-- Initial next reboot is now
next_reboot_time = 0

-- this is the function to reboot the router through telnet
function rebootrouter ()
    -- to send the command just once
    local reboot_sent = false
    local recv_count = 0

    -- receive data handler
    function recv(sck, data)
        -- debug stuff
        --recv_count = recv_count + 1
        --print(recv_count)
        print(data)

        -- Is it the line asking for login
        if string.find(data, SEARCH_USER, 1, true) ~= nil and string.find(data, SEARCH_USER, 1, true) > 0 then
            -- send telnet user name
            sck:send(USER)
            sck:send("\n")
        -- Is it the line asking for a password
        elseif string.find(data, SEARCH_PWD, 1, true) ~= nil and string.find(data, SEARCH_PWD, 1, true) > 0 then
            -- send telnet password            
            sck:send(PWD)
            sck:send("\n")
        -- Is it the user prompt; if yes we are logged in
        elseif string.find(data, SEARCH_PROMPT, 1, true) ~= nil and string.find(data, SEARCH_PROMPT, 1, true) > 0 then
            --if we did not already sent the reboot command, send it
            if reboot_sent ~= true then
                sck:send(REBOOT)
                sck:send("\n")
                --socket:send("reboot" .. "\n")
                -- debug stuff
                --sck:send("ls -al")
                --sck:send("\n")
                reboot_sent = true
            else
                sck:close()
            end
        end    
    end
    
    -- on connected handler
    function connected(sck, data)
        print("We are connected")
    end
    
    -- create a client socket
    print("REBOOT !!!")
    socket = net.createConnection(net.TCP, 0)
    
    -- register handlers
    socket:on("connection", connected)
    socket:on("receive", recv)
    
    -- connect to router on telnet port
    socket:connect(23, ROUTERIP)
    
    -- debug stuff
    print(socket:getpeer())
    --print(socket:getaddr())

    -- Update counter
    reboot_count = reboot_count -1
    if reboot_count < 0 then reboot_count = 0 end

    -- update next reboot time
    next_reboot_time = tmr.time() + reboot_period
end

-- Update state machine with connection status
function networkstatus ()
    -- Are we status GOT_IP if not status is not connected
    if wifi.sta.status() == 5 then
        MODE = 2
    else
        MODE = 1
    end
    
    -- debug stuff 
    --print(wifi.sta.status())
end

-- function to check if it is time to reboot and if yes, bounce it
function checkreboot()
    -- Reboot if connected, still reboot_count and delay between reboot passed
    if MODE == 2 and
        reboot_count > 0 and
        tmr.time() > next_reboot_time then
        rebootrouter ()
    elseif reboot_count == 0 then
        --We are done, diconnect to save battery
        wifi.sta.disconnect()
        MODE = 3 
    end
end

