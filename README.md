# README #

This project is the firmware code for the RouterRebootButton project.

Full project description on [hackster.io][]
[hackster.io]: https://www.hackster.io/jmjulien/router-reboot-button-0df78d

### What is this repository for? ###

At the heart of the project is a Huzzah board which host the NodeMCU firmware.
This repository hosts the lua code scripts for the NodeMCU firmware.

### How do I get set up? ###

* Read the Huzzah documentation : [huzzah][]
* NodeMCU : [NodeMCU][]
* Read the NodeMCU documentation : [docs][]
* Tools to put your scripts on the platform : [tools][]

[huzzah]: https://learn.adafruit.com/adafruit-huzzah-esp8266-breakout "Adafruit Huzzah"
[NodeMCU]: http://www.nodemcu.com/index_en.html "NodeMCU"
[docs]: https://nodemcu.readthedocs.io/en/master/ "NodeMCU Documentation"
[tools]: https://nodemcu.readthedocs.io/en/master/en/upload/ "Uploading Scripts"

### Who do I talk to? ###

Jean-Michel Julien
<jean-michel.julien@cadransys.com>